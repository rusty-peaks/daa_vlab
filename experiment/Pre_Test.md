### Pre Test
1. Linear search find the target item in which order:
                        <br>
                        A.<input type="radio" name="but" id="rb11" onclick="click1();">&nbsp;Skipping one element.
                        <br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">&nbsp;Sequential frames.
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">&nbsp;Unordered
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">&nbsp;None of these
                        <br>
                        <p id = "p1"></p>
                        <br>
2. What is time complexity if the target element found at first place of list?
                        <br>
                        A. <input type="radio" name="but2" id="rb21" onclick="click2();">&nbsp;O(n)
                        <br>
                        B. <input type="radio" name="but2" id="rb22" onclick="click2();">&nbsp;O(1)
                        <br>
                        C. <input type="radio" name="but2" id="rb23" onclick="click2();">&nbsp;O(index of target element)
                        <br>
                        D. <input type="radio" name="but2" id="rb24" onclick="click2();">&nbsp;None of these
                        <br><br>
                        <p id = "p2"></p>
                        <br>
3. Linear search applied on which data structures?
                        <br>
                        A. <input type="radio" name="but3" id="rb31" onclick="click3();">&nbsp;Tree and Array
                        <br>
                        B. <input type="radio" name="but3" id="rb32" onclick="click3();">&nbsp;Tree and List
                        <br>
                        C. <input type="radio" name="but3" id="rb33" onclick="click3();">&nbsp;Array and List
                        <br>
                        D. <input type="radio" name="but3" id="rb34" onclick="click3();">&nbsp;None of these
                        <br><br>
                        <p id = "p3"></p>
                        <br>
4. Which of the following is a disadvantage of linear search?
                        <br>
                        A. <input type="radio" name="but3" id="rb41" onclick="click3();">&nbsp;Requires more space
                        <br>
                        B. <input type="radio" name="but3" id="rb42" onclick="click3();">&nbsp;Greater time complexities compared to other searching algorithms
                        <br>
                        C. <input type="radio" name="but3" id="rb43" onclick="click3();">&nbsp;Not easy to understand
                        <br>
                        D. <input type="radio" name="but3" id="rb44" onclick="click3();">&nbsp;Not easy to implement
                        <br><br>
                        <p id = "p3"></p>
                        <br>