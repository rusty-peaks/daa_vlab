### References
<p style="font-size:100%; margin-top:2%">
                        1.&nbsp; <b>Narasimha Karumanchi </b>Data Structures and Algorithms Made Easy : Data Structure and Algorithm
                        <br><br>
                        2.&nbsp;<b>THOMAS H. CORMEN. (2014)</b>, Inroduction to Algorithms.
                        <br><br>
                        3.&nbsp; <b>Michael T. Goodrich and Roberto Tamassia</b>, Data Structures and Algorithms in Java
                        <br><br>
                        <h3>Webliography :</h3>
                        <br>
                        1.&nbsp;https://www.geeksforgeeks.org/linear-search/
                        <br>
                        2.&nbsp;https://en.wikipedia.org/wiki/Linear_search
                        <br>
                        <br>
                        <h3>Additional video links :</h3>
                        <br>
                        1.&nbsp;https://youtu.be/4GPdGsB3OSc
                        <br>
                        2.&nbsp;https://youtu.be/cdqHS28UgOs
                  <!--Theory of experiment -->
                    </p>