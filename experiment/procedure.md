#### 4. Procedure:
<br>

What Students will do? |	What Simulator will do?	| Purpose of the task
:--|:--|:-:
Examine the simulator screen and take note of all the instructions. | Display all the simulator contents.  |<div align = "justify">  Display simulator interface.
Click on Start Experiment Button  | Start The Experiment  |<div align = "justify"> To start the experiment.
Click the Generate Algorithm Button  | Algorithm for Linear Search will be displayed.  |<div align = "justify"> To recall the algorithm.
Enter the input details for array.<br>Also enter the element to be searched.  | Values will be determined for the array. |<div align = "justify"> To apply the algorithm on desired data.
Click on Submit button.  | Memory will be allocated for array.  |<div align = "justify"> To apply the algorithm on desired data.
Click on Next button.  | Highlight the lines of algorithm according to it's flow of control.  |<div align = "justify"> To go through each line of code and understand it's working.
Click on Draw Graph Button  | Draw The Graph  |<div align = "justify"> To analyse running time complexity of algorithm for the entered values.
Answer The Questions That Will Appear On Your Journey.  | Automatically Putup Few Pitfalls.  |<div align = "justify"> To make the algorithm of Linear Search clear.
Click on Conclusion button.  | Display the conclusion section.  |<div align = "justify"> To conclude the experiment.