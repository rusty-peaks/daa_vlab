### Theory
 <h3>Principle:</h3>
                    Linear search is the process of finding the element within a list or array.This algorithm sequentially check each element in the list or array until the desired element not found or the whole list or array has been serched.
                    There are two postulates: <br>
                   <br>
                    <h3>Process of Linear search:</h3>
                    This algorithm sequntially check each item from the start of list to the end of list until the the target value not found,if the execution reach the end of list then algorithm stops with unsuccessful search.<br>
                    In the searching process if the target element found then algorithm stops it's execution with successful search.<br>
                    <h3>Psuedo Code:</h3>
                    Linear Search (Array a ,Target t)<br><br>
                    &nbsp; &nbsp;Step 1:  &nbsp; &nbsp;    Set i to 1<br><br>
                    &nbsp; &nbsp;&nbsp;<b>COMMENT</b>: here array or list starts from 1st index<br><br>
                    &nbsp; &nbsp;Step 2:  &nbsp; &nbsp;     if i > n then go to step 7<br><br>
                    &nbsp; &nbsp;Step 3:  &nbsp; &nbsp;     if A[i] = t then go to step 6<br><br>
                    &nbsp; &nbsp;Step 4:  &nbsp; &nbsp;     Set i to i + 1<br><br>
                    &nbsp; &nbsp;Step 5:  &nbsp; &nbsp;     Go to Step 2<br><br>
                    &nbsp; &nbsp;Step 6:  &nbsp; &nbsp;     Print Element t found at index i and go to step 8<br><br>
                    &nbsp; &nbsp;Step 7:  &nbsp; &nbsp;     Print element not found<br><br>
                    &nbsp; &nbsp;Step 8:   &nbsp; &nbsp;    Exit<br><br>
                    <h3>Example 1:</h3>
                    <h4><b>Input array: </b> 45, 34, 67, 12, 9, 29, 7, 100</h4>
                    <h4><b>Target item: </b>12</h4>
                    <h4><b>Result: </b>&nbsp; &nbsp;Element 12 found at index 4</h4> <br>
                    <h3>Example 2:</h3>
                    <h4><b>Input array: </b> 45, 34, 67, 12, 9, 29, 7, 100</h4>
                    <h4><b>Target item: </b>120</h4>
                    <h4><b>Result: </b>&nbsp; &nbsp;element not found</h4> <br>
                    <h3>Running time of algorithm:</h3>
                    The <b>worst case</b> running time complexity of linear search is <b>O(n)</b> ,where n is the number of elements in array<br>each element is searched only one time and goes till last element.<br>
                    The <b>Best case</b> running time complexity of linear search is <b>O(1)</b> when the target element found on 1st index then algorithm stops on 1st step.