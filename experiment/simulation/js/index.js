const container = document.getElementById('container')
const input_cont = document.getElementById('input_container')
const array_size = document.getElementById('array_size')
const start = document.getElementById('Start_Experiment')
const generate = document.getElementById('Generate_Algorithm')
const analyse = document.getElementById('Analyse_Algorithm')
const algorithm = document.getElementById('Algorithm')
const graph = document.getElementById('graph')
const draw_graph = document.getElementById('draw_graph')

const algo = []
const arr = []
var item


window.onload = ()=>{
    array_size.disabled = true
    draw_graph.style.display = 'none'
}

const userName = prompt('Please enter your name.')

start.addEventListener('click',()=>{
    array_size.disabled = false
    let label = document.createElement("label")
    label.id = "welcome"
    label.className = "welcome"
    label.innerHTML = 'Welcome '+`${userName} Let's understand the Linear Search algorithm together.`
    input_cont.append(label)
})

array_size.addEventListener('focusin',()=>{
    let label = document.getElementById('label1')
    label.innerHTML = 'Hit Enter'    
})

array_size.addEventListener('focusout',()=>{
    let label = document.getElementById('label1')
    label.innerHTML = 'Array Size'
})

array_size.addEventListener('change',()=>{
    let len = validateInput()
    if(len >= 0){
        item = getElement(0)

        let division = document.createElement('div')
        division.id = 'item_container'
        division.className ='item_container'
        container.append(division)

        let item_cont = document.getElementById('item_container')
        let header =  document.createElement('h5')
        header.innerHTML = 'Item'

        division = document.createElement('div')
        division.id = 'item'
        division.className = 'item'
        division.innerHTML = item

        item_cont.append(header)
        item_cont.append(division)

        header = document.createElement("label")
        header.id = "array_heading"
        header.className = "array_heading"
        header.innerHTML = "Array"
        container.append(header)
        for (let i = 1; i <= len; i++) {
            let division = document.createElement('div')
            division.id = 'cir' + i
            division.className = 'cir' + i
            let element = getElement(i)
            division.innerHTML = element
            container.append(division)
            arr.push(element)
        }

        alert('Array created successfully.')


        generate.style.display = 'block'
    }
})

generate.addEventListener('click',()=>{
    makeAlgorithm()

    for (let i = 0; i < algo.length; i++) {
        let step = document.createElement('h3')
        step.id = "h3" + i
        step.innerHTML = algo[i]
        algorithm.append(step)
    }
    alert('Algorithm successfully generated.\n Please look for \'Analyse Algorithm\' button on the screen.')
    analyse.style.display = 'block'
    generate.disabled = true
})

var step = 0
var item_found = false
var for_count = 0
var total_cost = 0

analyse.addEventListener('click', ()=>{
    analyse.innerHTML = 'Next Step'
    nextStep()
})


//Draw Graph Trigger
draw_graph.addEventListener('click', ()=>{
    plot_graph(arr.length, total_cost);
})

function trigger_for(){
    //for loop color reset
    let id = 'h3' + 1
    let line = document.getElementById(id)
    line.style.transition = '100ms'
    line.style.color = 'black'
    console.log('Inside For Loop', for_count, ' Element is: ', arr[for_count], ' item: ', item)

    //if condition color set
    id = 'h3' + 2
    line = document.getElementById(id)
    line.style.transition = '100ms'
    line.style.color = 'red'
    if(arr[for_count] == item){
        //if condition color reset
        line = document.getElementById(id)
        line.style.transition = '100ms'
        line.style.color = 'black'

        total_cost += 1

        let cir_id = 'cir' + (for_count + 1)
        let elem = document.getElementById(cir_id)
        elem.style.animation = "found 2.5s ease-in-out 0s 1 forwards"
        elem.style.webkitAnimation = "found 2.5s ease-in-out 0s 1 forwards"

        //printf color set
        id = 'h3' + 3
        line = document.getElementById(id)
        line.style.transition = '100ms'
        line.style.color = 'red'
        line.style.color = 'black' //printf color reset

        //return color set
        id = 'h3' + 4
        line = document.getElementById(id)
        line.style.transition = '100ms'
        line.style.color = 'red'
        line.style.color = 'black'


        item_found = true
        return
    }
    //CSS animation triggering
    let cir_id = 'cir' + (for_count + 1)
    let elem = document.getElementById(cir_id)
    elem.style.animation = "notfound 2.5s ease-in-out 0s 1 forwards"
    elem.style.webkitAnimation = "notfound 2.5s ease-in-out 0s 1 forwards"

    total_cost += 1
    for_count += 1
    return
}

function nextStep(){
    console.log('Value Of step is: ',step)
    switch(step){
        case 0:{
            let line = document.getElementById('h3'+step)
            line.style.transition = '200ms'
            line.style.color = 'red'
            step += 1
            break;
        }
        case 1:{
            let line = document.getElementById('h3' + 0)
            line.style.transition = '200ms'
            line.style.color = 'black'
            let len = arr.length
            if(!item_found && for_count<len){
                //for loop color set
                let id = 'h3' + 1
                line = document.getElementById(id)
                line.style.transition = '100ms'
                line.style.color = 'red'
                console.log('Inside For Loop', for_count, ' Element is: ', arr[for_count], ' item: ', item)
                trigger_for(for_count)

                //if condition color reset
                id = 'h3' + 2
                line = document.getElementById(id)
                line.style.transition = '100ms'
                line.style.color = 'black'

                //for loop color set
                id = 'h3' + 1
                line = document.getElementById(id)
                line.style.transition = '100ms'
                line.style.color = 'red'

                if(!item_found)
                    break
            }
            if (item_found) {
                alert('You have found the item.\nReset to do this part again.\n\nFinish the algorithm tracing and press \'Draw Graph\' to plot graph.')
            }
            step += 1
            break;
        }
        case 2:{
            //Element not found
            //for reset color
            let id  = 'h3'+1
            document.getElementById(id).style.transition = '200ms'
            document.getElementById(id).style.color = 'black'

            //printf color
            let line = document.getElementById('h3'+6)
            line.style.transition = '200ms'
            line.style.color = 'red'
            step += 1
            break;
        }
        case 3:{
            let id  = 'h3'+6
            document.getElementById(id).style.transition = '200ms'
            document.getElementById(id).style.color = 'black'
            let line = document.getElementById('h3'+7)
            line.style.transition = '200ms'
            line.style.color = 'red'
            step += 1
            break;
        }
        default:{
            let line = document.getElementById('h3' + 7)
            line.style.transition = '200ms'
            line.style.color = 'black'
            draw_graph.style.display = 'block'
            if(!item_found)
                alert('You item is not present in the array.\nProceed with the experiment to plot graph.\n\nReset to do this step again.')
            break;
        }
    }
    if (item_found && step>4) {
        alert('You have already found the item.\n')
        return
    }
    return
}

function makeAlgorithm() {
    algo.push("function linear_search(array, item){")
    algo.push("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for(int i = 0; i < array.length; i++){")
    algo.push("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if(array[i] == item){")
    algo.push("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;printf('element found');")
    algo.push("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return;}")
    algo.push("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}")
    algo.push("printf('Element not found');")
    algo.push("return;")
    algo.push("}")
}

function getElement(n){
    try {
        if(n == 0){
            let element = parseInt(prompt(`Please Enter Item To Search For`))
            if(isNaN(element))
                throw new Error("NAN Item")
            return element
        }
        let element = parseInt(prompt(`Please enter ${n} element`))
        if(isNaN(element))
            throw new Error("NAN Element")
        return element
    } catch (error) {
        console.log(error)
        alert('Please provide valid input')
        return getElement(n)
    }
}

function validateInput(){
    let size = parseInt(array_size.value)
    if(size>10){
        alert('Please enter the size of array.\n\nKeep the array size under 10 for smooth animation.')
        array_size.value = 0
        return -1
    }
    return size
}

var data = [];
function plot_graph(n,cost)
{
    console.log("Array length: ",n," Total_cost", cost)
    if(cost==1)
    {
        for(var i=0;i<=n;i++)
        {
            data.push({x:i, y:1});
        }
    }
    else{
        for(var i=0;i<=cost;i++)
        {
            data.push({x:i,y:i});
        }
    }
    graphline("graph", data, "Number of elements --->", "Cost --->"); 
}