### Aim
 To analyze and implement the algorithm of linear search.
### 
 <h3>Principle:</h3>
                    Linear search is the process of finding the element within a list or array.This algorithm sequentially check each element in the list or array until the desired element not found or the whole list or array has been serched.
                    There are two postulates: <br>
                   <br>
                    <h3>Process of Linear search:</h3>
                    This algorithm sequntially check each item from the start of list to the end of list until the the target value not found,if the execution reach the end of list then algorithm stops with unsuccessful search.<br>
                    In the searching process if the target element found then algorithm stops it's execution with successful search.<br>
                    <h3>Psuedo Code:</h3>
                    Linear Search (Array a ,Target t)<br><br>
                    &nbsp; &nbsp;Step 1:  &nbsp; &nbsp;    Set i to 1<br><br>
                    &nbsp; &nbsp;&nbsp;<b>COMMENT</b>: here array or list starts from 1st index<br><br>
                    &nbsp; &nbsp;Step 2:  &nbsp; &nbsp;     if i > n then go to step 7<br><br>
                    &nbsp; &nbsp;Step 3:  &nbsp; &nbsp;     if A[i] = t then go to step 6<br><br>
                    &nbsp; &nbsp;Step 4:  &nbsp; &nbsp;     Set i to i + 1<br><br>
                    &nbsp; &nbsp;Step 5:  &nbsp; &nbsp;     Go to Step 2<br><br>
                    &nbsp; &nbsp;Step 6:  &nbsp; &nbsp;     Print Element t found at index i and go to step 8<br><br>
                    &nbsp; &nbsp;Step 7:  &nbsp; &nbsp;     Print element not found<br><br>
                    &nbsp; &nbsp;Step 8:   &nbsp; &nbsp;    Exit<br><br>
                    <h3>Example 1:</h3>
                    <h4><b>Input array: </b> 45, 34, 67, 12, 9, 29, 7, 100</h4>
                    <h4><b>Target item: </b>12</h4>
                    <h4><b>Result: </b>&nbsp; &nbsp;Element 12 found at index 4</h4> <br>
                    <h3>Example 2:</h3>
                    <h4><b>Input array: </b> 45, 34, 67, 12, 9, 29, 7, 100</h4>
                    <h4><b>Target item: </b>120</h4>
                    <h4><b>Result: </b>&nbsp; &nbsp;element not found</h4> <br>
                    <h3>Running time of algorithm:</h3>
                    The <b>worst case</b> running time complexity of linear search is <b>O(n)</b> ,where n is the number of elements in array<br>each element is searched only one time and goes till last element.<br>
                    The <b>Best case</b> running time complexity of linear search is <b>O(1)</b> when the target element found on 1st index then algorithm stops on 1st step. 
                    
                


### Pre Test
1. Linear search find the target item in which order:
                        <br>
                        A.<input type="radio" name="but" id="rb11" onclick="click1();">&nbsp;Skipping one element.
                        <br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">&nbsp;Sequential frames.
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">&nbsp;Unordered
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">&nbsp;None of these
                        <br>
                        <p id = "p1"></p>
                        <br>
2. What is time complexity if the target element found at first place of list?
                        <br>
                        A. <input type="radio" name="but2" id="rb21" onclick="click2();">&nbsp;O(n)
                        <br>
                        B. <input type="radio" name="but2" id="rb22" onclick="click2();">&nbsp;O(1)
                        <br>
                        C. <input type="radio" name="but2" id="rb23" onclick="click2();">&nbsp;O(index of target element)
                        <br>
                        D. <input type="radio" name="but2" id="rb24" onclick="click2();">&nbsp;None of these
                        <br><br>
                        <p id = "p2"></p>
                        <br>
3. Linear search applied on which data structures?
                        <br>
                        A. <input type="radio" name="but3" id="rb31" onclick="click3();">&nbsp;Tree and Array
                        <br>
                        B. <input type="radio" name="but3" id="rb32" onclick="click3();">&nbsp;Tree and List
                        <br>
                        C. <input type="radio" name="but3" id="rb33" onclick="click3();">&nbsp;Array and List
                        <br>
                        D. <input type="radio" name="but3" id="rb34" onclick="click3();">&nbsp;None of these
                        <br><br>
                        <p id = "p3"></p>
                        <br>
3. Which of the following is a disadvantage of linear search?
                        <br>
                        A. <input type="radio" name="but3" id="rb41" onclick="click3();">&nbsp;Requires more space
                        <br>
                        B. <input type="radio" name="but3" id="rb42" onclick="click3();">&nbsp;Greater time complexities compared to other searching algorithms
                        <br>
                        C. <input type="radio" name="but3" id="rb43" onclick="click3();">&nbsp;Not easy to understand
                        <br>
                        D. <input type="radio" name="but3" id="rb44" onclick="click3();">&nbsp;Not easy to implement
                        <br><br>
                        <p id = "p3"></p>
                        <br>
                       

### Post Test
<p style="font-size:100%; margin-top:2%">
                        1. What is linear search and how does it work?
                        <br>
                        2. What is the running time of linear search algorithm in best and worst case?
                        <br>
                        3. What is the output of above psuedo code if given list is {4,6,3,8,6,1,0}  and target element is 1 and how?
                        <br>
                        <br>
                    <!--Post Test of experiment -->
                    </p>


### References
<p style="font-size:100%; margin-top:2%">
                        1.&nbsp; <b>Narasimha Karumanchi </b>Data Structures and Algorithms Made Easy : Data Structure and Algorithm
                        <br><br>
                        2.&nbsp;<b>THOMAS H. CORMEN. (2014)</b>, Inroduction to Algorithms.
                        <br><br>
                        3.&nbsp; <b>Michael T. Goodrich and Roberto Tamassia</b>, Data Structures and Algorithms in Java
                        <br><br>
                        <h3>Webliography :</h3>
                        <br>
                        1.&nbsp;https://www.geeksforgeeks.org/linear-search/
                        <br>
                        2.&nbsp;https://en.wikipedia.org/wiki/Linear_search
                        <br>
                        <br>
                        <h3>Additional video links :</h3>
                        <br>
                        1.&nbsp;https://youtu.be/4GPdGsB3OSc
                        <br>
                        2.&nbsp;https://youtu.be/cdqHS28UgOs
                  <!--Theory of experiment -->
                    </p>
