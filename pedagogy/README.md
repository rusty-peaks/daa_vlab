## Round 1
<p align="center">

<b> Experiment : Linear Search </b> <a name="top"></a> <br>
</p>

<b>Discipline | </b> Computer Science
:--|:--|
<b> Lab</b> | Design and Analysis of Algorithms Lab
<b> Experiment</b>|1. Linear Search


<h4> [1. Focus Area](#LO)
<h4> [2. Learning Objectives ](#LO)
<h4> [3. Instructional Strategy](#IS)
<h4> [4. Task & Assessment Questions](#AQ)
<h4> [5. Simulator Interactions](#SI)
<hr>

<a name="LO"></a>
#### 1. Focus Area : Reinforce theoretical concept and Experimentation
#### 2. Learning Objectives and Cognitive Level


Sr. No |	Learning Objective	| Cognitive Level | Action Verb
:--|:--|:--|:-:
1.| User will be able to: <br>recall the algorithm for Linear Search. | Remembering | Recall
2.| User will be able to: <br>understand the algorithm of Linear Search by visualizing it's working.| Understanding| Demonstrate
3.| User will be able to: <br>apply the algorithm using custom data(values of choice for the array). | Applying | Implement
4.| User will be able to: <br>To analyze the running time complexity of the algorithm.| Analysing| Analyse




<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="IS"></a>
#### 3. Instructional Strategy
###### Name of Instructional Strategy  :     <u> Expository and problem based.</u>
###### Assessment Method: <u>Formative Assessment and Summative Assessment</u>

<u> <b>Description: </b> In this experiment you will learn about Linear Search algorithm and will do it's analysis. </u>
<br>
 <div align="justify">•	The main objective to develop this lab is to provide an interactive source of learning for the students. The simulation that we provide fulfills our purpose.
<br>
•	The learner will be easily able to understand Linear Search algorithm.<br>
•	The user will able to apply the algorithm on his choice of data.<br>
•	With the help of this experiment, students get a chance to analyse the algorithm of Linear Search and plot a graph for desired values.
<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="AQ"></a>
#### 4. Task & Assessment Questions:

Read the theory and comprehend the concepts related to the experiment.
<br>

Sr. No |	Learning Objective	| Task to be performed by <br> the student  in the simulator | Assessment Questions as per LO & Task
:--|:--|:--|:-:
1.| Recall | To recall the algorithm of Linear Search. | <div align="justify"> Which of these statements are true for Linear Search?<br> i. Searching takes place sequentially.<br> ii. Searching takes place in intervals.<br> iii. Searching takes place only for sorted data.<br> iv. None of these
2.| Understand | To understand the working of Linear Search algotithm. | <div align="justify">Which of the following  is true for Linear Search?<br> i. It is the best searching algorithm <br> ii. It makes n comparisons for n elements.<br> iii. Both i and ii are correct<br> iv. None of these
3.| Apply | Apply Linear Search algorithm on custom values. |<div align="justify"> From which index does Linear Search starts comparison?<br>i. 0<br>ii. last<br>iii. unknown<br>iv. None of these
4.| Analyze | To analyze the running time complexity of Linear Search algorithm. | Given an array A = {1,5,3,2}. We are searching the<br>element 1. What case is this?<br> i. Best Case<br> ii. Average Case<br> iii. Worst Case<br> iv. None of these
 <br>

 <br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="SI"></a>

#### 4. Simulator Interactions:
<br>

What Students will do? |	What Simulator will do?	| Purpose of the task
:--|:--|:-:
Examine the simulator screen and take note of all the instructions. | Display all the simulator contents.  |<div align = "justify">  Display simulator interface.
Click on Start Experiment Button  | Start The Experiment  |<div align = "justify"> To start the experiment.
Click the Generate Algorithm Button  | Algorithm for Linear Search will be displayed.  |<div align = "justify"> To recall the algorithm.
Enter the input details for array.<br>Also enter the element to be searched.  | Values will be determined for the array. |<div align = "justify"> To apply the algorithm on desired data.
Click on Submit button.  | Memory will be allocated for array.  |<div align = "justify"> To apply the algorithm on desired data.
Click on Next button.  | Highlight the lines of algorithm according to it's flow of control.  |<div align = "justify"> To go through each line of code and understand it's working.
Click on Draw Graph Button  | Draw The Graph  |<div align = "justify"> To analyse running time complexity of algorithm for the entered values.
Answer The Questions That Will Appear On Your Journey.  | Automatically Putup Few Pitfalls.  |<div align = "justify"> To make the algorithm of Linear Search clear.
Click on Conclusion button.  | Display the conclusion section.  |<div align = "justify"> To conclude the experiment.