## Round 2

Experiment 1: Linear Search

### 1. Story Outline:

<div align="justify"> In computer science, a linear search or sequential search is a method for finding an element within a list. It sequentially checks each element of the list until a match is found or the whole list has been searched.
A linear search runs in at worst linear time and makes at most n comparisons, where n is the length of the list. If each element is equally likely to be searched, then linear search has an average case of 
n/2 comparisons, but the average case can be affected if the search probabilities for each element vary.
Linear search is rarely practical because other search algorithms and schemes, such as the binary search algorithm and hash tables, allow significantly faster searching for all but short lists.

### 2. Story:

#### 2.1 Set the Visual Stage Description:
<h2>Construction of the set-up</h2>

For better visualization, a simulator is provided. Linear Search is taught in a theoretical manner but there is always a need of some physical significance. Since performing this experiment in real life is possible only through a code and there is no clear explanation of algorithm, so it becomes difficult for a student to understand the algorithm. Four sections are provided one each for input, animation, graph, algorithm. 

#### 2.2 Set User Objectives & Goals:
Sr. No |	Learning Objective	| Cognitive Level | Action Verb
:--|:--|:--|:-:
1.| User will be able to: <br>recall the algorithm for Linear Search. | Remembering | Recall
2.| User will be able to: <br>understand the algorithm of Linear Search by visualizing it's working.| Understanding| Demonstrate
3.| User will be able to: <br>apply the algorithm using custom data(values of choice for the array). | Applying | Implement
4.| User will be able to: <br>To analyze the running time complexity of the algorithm.| Analysing| Analyse

Enhance conceptual and logical skill
</b>

#### 2.3 Set the Pathway Activities:

The simulator tab would allow:<br> <br>
<dd>
1.	The set-up consists of four sections; one each for input, animation, algorithm, graph.<br>
2.	Input fields to take array values and prompts to take searching element.<br>
3.	There would be a ‘Start Experiment’ button which will start the working of simulator.<br>
4.	You can generate the algorithm using 'Generate Algorithm' button.<br>
5.	“Graph” button allows you to plot a graph between number of steps and number of elements.<br>
6.	Answer the pitfall questions to proceed through the experiment.<br>
</dd>


##### 2.4 Set Challenges and Questions/Complexity/Variations in Questions:

Assessment Questions:<br>
Task 1: Recalling the concept of Linear Search<br>

<dd><b>Which of these statements are true for Linear Search?<br>
a. Searching takes place sequentially.<br>
b. Searching takes place in intervals.<br>
c. Searching takes place only for sorted data.<br>
d. None of these<br></dd><br></b>
Task 2 : To understand the algorithm of Linear Search.<br><br>
<dd><b>1.Which of the following is true for Linear Search?<br>
a. It is the best searching algorithm <br>
b. It makes n comparisons for n elements.<br>
c. Both i and ii are correct<br>
d. None of these
<br><br></b>
</dd>
Task 3 : Apply Linear Search Algorithm<br><br>
<dd><b>From which index does Linear Search starts comparison?<br>
a. 0<br>
b. last<br>
c. unknown<br>
d. None of these</dd></b><br>
Task 4 : To analyze the Linear Search Algorithm<br><br>
<dd>
<b> Given an array A = {1,5,3,2}. We are searching the element 1. What case is this?<br>
a. Best Case<br>
b. Average Case<br>
c. Worst Case<br>
d. None of these <br></b>
</dd>


##### 2.5 Allow pitfalls:
<dd>i.	User will have to solve a question to proceed further in the experiment.<br>
ii.	At two instances, such questions will be asked so that the user can test his knowledge.<br>
iii. MCQ questions have to be answered to enable simulator buttons.<br>
</dd>


##### 2.6 Conclusion:
<dd>Linear Search algorithm is a sequential searching algorithm which searches an element in an array in a contiguous linear way. There are no pre-requisites to perform this algorithm which makes it easy to implement. But since it has O(n) time complexity, it is not preferred 
</dd>

##### 2.7 Equations/formulas: NA


### 3. Flowchart
<img src="flowchart/flowchart.png" alt="Flow Chart Image here"/>

### 4. Mindmap
<img src="mindmap/mindmap.png" alt="mindmap Image here"/>
 
### 5. Storyboard 
<img src="storyboard/storyboard.gif" alt="Gif here">
