
<b>Discipline | <b>Computer Science
:--|:--|
<b>Lab</b> | <b>Design and Analysis of Algorithms Lab</b>
<b>Experiment</b>| <b>1. Linear Search</b>

<h5> About the Experiment : </h5>
In this experiment the user understands about a famous searching algortihm, Linear Search. With the help of this experiment, students get a chance to analyse the algorithm of Linear Search and plot a graph for desired values. 

<b>Name of Developer | <b> Kartika Pradeep
:--|:--|
Institute | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh
Email id| kartikapradeep04@gmail.com
Department | Dept. of Computer Science


#### Contributors List

S. No | Name | Faculty or Student | Department| Institute | Email id
:--|:--|:--|:--|:--|:--|
1 | Kartika Pradeep | Faculty | Dept. of Computer Science | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | kartikapradeep04@gmail.com
2 | Pawan Yadav | Student | Dept. of Information Technology | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | pkpawan954@gmail.com
3 | Yash Srivastava | Student | Dept. of Information Technology | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | officialyash2616@gmail.com
4 | Mohit Rajput | Student | Dept. of Information Technology | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | raj.mohit198@gmail.com
5 | Shantanu Gupta | Student | Dept. of Information Technology | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | officialshantanugupta08@gmail.com
